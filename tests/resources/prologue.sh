#!/bin/sh

# remember where the original workspace is
ORIG_WKP=$PWD

# move to squash-tm actual home
cd /opt/squash-tm

# wait for the database to be up and ready
until mysqladmin  --protocol tcp status -u root -proot; do echo waiting for mysql; sleep 2; done;
until pg_isready -h localhost -d squashtm -U squashtm -t 5; do echo waiting for postgresql; sleep 2; done;

# let squash-tm init with H2 we don't care (yet), here we abuse the tooling and assets to generate the DB ourselves
# Mysql :
echo 'Full install MySQL'
FULL_INSTALL_FILE=$(find . -name "*mysql*full*sql")
mysql  --protocol tcp -u squashtm -psquashtm --default-character-set=utf8 squashtm < $FULL_INSTALL_FILE

# Pg :
echo 'Full install Postgresql'
FULL_INSTALL_FILE=$(find . -name "*postgres*full*sql")
psql postgresql://root:squashtm@localhost/squashtm -f $FULL_INSTALL_FILE > /dev/null 2>&1

# exporting the results
echo 'Collecting the DATABASECHANGELOG ids for both bases'
mysql --protocol tcp -u root -proot squashtm \
      -e 'select ID from DATABASECHANGELOG order by ORDEREXECUTED' > "$ORIG_WKP/ids-mysql"

psql postgresql://root:squashtm@localhost/squashtm \
      -c 'select ID from DATABASECHANGELOG order by ORDEREXECUTED' > "$ORIG_WKP/ids-postgres"


