
// This file is created by 'install-script.sh' when the variables are resolved,
// and before the DB update routine fires up.
DOCKER_RESOLVED_ENV='/opt/squash-tm/bin/docker-resolved-env'

mysql_old_squash_script = readFile "${workspace}/tests/resources/mysql-full-install-version-1.16.0.RELEASE.sql"
postgres_old_squash_script = readFile "${workspace}/tests/resources/postgresql-full-install-version-1.16.0.RELEASE.sql"


// ************ Generic commands ***************************

// This will run the script 'scriptTxt' after having sourced DOCKER_RESOLVED_ENV so that the
// environment variables are available
def ctxsh(scriptTxt){
    sh """. ${DOCKER_RESOLVED_ENV}
        ${scriptTxt}
"""
}

def declareHost(hostname){
    sh "echo '127.0.0.1 $hostname' >> /etc/hosts"
}

def waitUrl(url){
    sh "until wget -q $url > /dev/null 2>&1; do sleep 10; done"
}

def waitTMAlive(){
    waitUrl('http://localhost:8080/squash/isSquashAlive')
}

def waitConfPublished(){
    // remember : DOCKER_RESOLVED_ENV is groovy variable, not a shell variable
    sh "until [ -f $DOCKER_RESOLVED_ENV ]; do sleep 3; done"
}

def manualStart(){
    sh 'cd /opt && nohup sh install-script.sh &'
}

def configurationIs(cfgTxt){
    sh """
    echo '$cfgTxt' > expected-cfg
    diff $DOCKER_RESOLVED_ENV expected-cfg
    """
}


// ************* MySQL-related commands ***************************

def dumpMysqlChlogIdsInto(outputfile){
    ctxsh ('''
        mysql --protocol tcp -h $DB_HOST -P ${DB_PORT} -u $DB_USERNAME -p$DB_PASSWORD $DB_NAME \
                -e "select ID from DATABASECHANGELOG order by ORDEREXECUTED" > ''' + outputfile)
}

def mysqlIsUpToDate(){
    dumpMysqlChlogIdsInto('ids-actual')
    unstash 'db-ids'
    sh 'diff ids-mysql ids-actual'
}

// warning : this method assumes the default mysql parameters
// because it is usually called while docker_resolved_env doesn't exist yet
def loadOldMysql(){
    writeFile file: 'mysql_old_squash.sql', text: mysql_old_squash_script
    sh('''
        mysql --protocol tcp -h mysql -u squashtm -psquashtm squashtm < mysql_old_squash.sql
    ''')
}
def waitMysql(){
    sh 'until mysqladmin  --protocol tcp status -u squashtm -psquashtm; do echo waiting for mysql; sleep 2; done;'
}

// ************* PG-related commands ***********************

def dumpPGChlogIdsInto(outputfile){
    sh '''
        . /opt/squash-tm/bin/docker-resolved-env
        PGOPTIONS="--search_path=${DB_SCHEMA}"  psql postgresql://$DB_USERNAME:$DB_PASSWORD@$DB_HOST:${DB_PORT}/$DB_NAME \
                -c "select ID from DATABASECHANGELOG order by ORDEREXECUTED" > ''' + outputfile
}



def PGisUpToDate(){
    dumpPGChlogIdsInto('ids-actual')
    unstash 'db-ids'
    sh 'diff ids-postgres ids-actual'
}

// warning : this method assumes the default postgres parameters
// because it is usually called while docker_resolved_env doesn't exist yet
def loadOldPG(){
    writeFile file: 'postgres_old_squash.sql', text: postgres_old_squash_script
    sh('''
        psql -h postgres -p 5432 -U root -d squashtm -f postgres_old_squash.sql
    ''')
}
// warning : this method assumes the default postgres parameters
// because it is usually called while docker_resolved_env doesn't exist yet
def waitPostgresql(){
    sh 'until pg_isready -h postgres -p 5432 -U root; do echo waiting for pgsql; sleep 2; done;'
}


return this