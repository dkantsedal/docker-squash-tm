﻿# What is Squash TM ?

Squash TM is one of the best open source management test tools for test repository management.
It has been designed for sharing test projects between stakeholders.

**Squash TM offers the following features:**

- Requirements, Test cases, Campaigns management
- Report management: custom reports (charts, dashboards) for requirements
- Connection with Bugtrackers and Agile project management tool: Jira
- Automation workspace for developers and testers introducing a Gherkin editor

**Squash TM also offers specific features designed for collaborative work:**

- Multi- and inter- project possibilities
- "Full web" tool, which does not need to be set up on the client workstation
- Intuitive and ergonomic design, RIA (Rich Internet Application) technology
- Light administration & user management

All documentation is available here: [https://sites.google.com/a/henix.fr/wiki-squash-tm/](https://sites.google.com/a/henix.fr/wiki-squash-tm/)

# Supported tags and Dockerfile links

The Dockerfile builds from "openjdk:8-jre-alpine" see [https://hub.docker.com/_/openjdk](https://hub.docker.com/_/openjdk)

The container tagged `latest` will always refer to the last absolute stable version of Squash TM. Additionnaly starting from 
 Squash-TM 1.21 each release will also track its latest micro version with tags of the form `<major>.<minor>-latest`, in case you 
 are only interested with bugfixes for your specific version.
 
The image assets (Dockerfile and scripts) are available here : [https://bitbucket.org/squashtest/docker-squash-tm/src/master/build-context/](https://bitbucket.org/squashtest/docker-squash-tm/src/master/build-context/)

# Quick Start

Run the Squash-TM image with an embedded h2 database (for demo purpose only !)
```
docker run --name='squash-tm' -it -p 8090:8080 squashtest/squash-tm
```
*NOTE*: To run the container in daemon mode, use the -d option :

```
docker run --name='squash-tm' -d -it -p 8090:8080 squashtest/squash-tm
```
Please allow a few minutes for the applicaton to start, especially if populating the database for the first time. If you want to make sure that everything went fine, watch the log:
```
docker exec -d -p 8080:8080 --name squash-tm squashtest/squash-tm
docker logs squash-tm -f
```
Go to http://localhost:8090/squash or point to the IP of your docker host. 

The default username and password are:

-   username: **admin**
-   password: **admin**


# Configuration

*The following sections show how to deploy Squash TM using an external **PostgreSQL DB** container or an external **MariaDB** container. Exemples of yml file also show how to deploy this solution using **docker-compose**.*

## UID and GID

As of Squash-TM 1.21.4 and onward, the main process will run as `uid=100` and `gid=101` (known as `squashtm:squashtm` within the container ).
In earlier version that user is root.

## Backing up data with persistent volumes

As you may already know, in Docker you would generally keep data in volumes.

So, in order to use Squash TM image properly, it is highly recommended to set up an external database (MariaDB or PostgreSQL).

Each of these configurations provide the creation of a persistant volume for data.
```
/var/lib/postgresql/data     # Data location using PostgreSQL
/var/lib/mysql               # Data location using MariaDB
```
Moreover, if your purpose is to use squash TM image in production, you'll probably want to persist the following location in a volume in order to keep traces of logs.
```
/opt/squash-tm/logs          # Log directory
```
For more info check Docker docs section on [Managing data in containers](https://docs.docker.com/engine/tutorials/dockervolumes/)


## Environment variables

### Database access

As of TM Version 1.21.4 and onward the following variables replace the former ones.
For earlier versions please read `Deprecated Environment Variables` further below.

| Variable        | Description                           | Default H2  | Default MySQL | Default Postgresql  |
| ----------------|:-------------------------------------:|:-----------:|:-------------:|:-------------------:|
| SQTM_DB_TYPE    | Either 'h2', 'mysql' or 'postgresql'  | (special)   | (special)     | (special)           |
| SQTM_DB_HOST    | Hostname of the database server       | (unused)    | mysql         | postgres            |
| SQTM_DB_PORT    | Port of the database sever            | (unused)    | 3306          | 5432                |
| SQTM_DB_NAME    | The name of the database              | (unused)    | squashtm      | squashtm            |
| SQTM_DB_SCHEMA *| The name of the schema                | (unused)    | $DB_NAME      | public              |
| SQTM_DB_USERNAME| The username for Squash-TM            | (unused)    | root          | root                |
| SQTM_DB_PASSWORD| The password for Squash-TM            | (unused)    | (none)        | (none)              |

Notes :
- (none) : the variable is mandatory and has no default value.
- (special) : see below.
- (*) : experimental, should be stable but has not yet gone through thorough testing.
- (unused) :  the variable has no meaning for this database server. In the case of H2 those values are hardcoded to Squash-TM default internal parameters.

### Variable SQTM_DB_TYPE and deprecated variables

`SQTM_DB_TYPE` has an impact on the default values for several others. The best practice is to define it explicitly.
Be careful to type in the intended value correctly (eg `postgresql` and not 'postgres'), otherwise the application 
will default to `h2` and might still boot successfully - yet not the way you intended.    

In previous versions of the image this variable was internal only and its value was implied by the existence
of other variables which are now deprecated (see below).
This mechanism is still functional : basically if `SQTM_DB_TYPE` is undefined but any variable of the form
`MYSQL_ENV_*` or `POSTGRES_ENV_*` is configured then the value of `SQTM_DB_TYPE` will be implicitly resolved. 
However, variables
of the form `SQTM_*` will take precedence. Also be aware that this secondary mechanism will effectively stop working
once the deprecated variables are removed eventually.

### Deprecated Environment Variables

Versions 1.21.1 and earlier of this image defined a different set of variables which are now deprecated. 
They are still accepted by newer images, therefore **your former deployment descriptor will still work** 
until further notice. Still the variables listed in the table above remain the preferred way to configure it.

The only exception is `MYSQL_ENV_MYSQL_ROOT_PASSWORD`, which has been definitely removed and is not supported anymore. 

When one of these deprecated variable is conflicting with its equivalent newer variable, the new variable will take precedence. 

#### Deprecated MySQL/MariaDB-specific Environment variables :
- `MYSQL_ENV_MYSQL_ROOT_PASSWORD` : definitely removed
- `MYSQL_ENV_MYSQL_USER` : equivalent of (and superseded by) `SQTM_DB_USERNAME`
- `MYSQL_ENV_MYSQL_PASSWORD` : equivalent of (and superseded by) `SQTM_DB_PASSWORD`
- `MYSQL_ENV_MYSQL_DATABASE` : equivalent of (and superseded by) `SQTM_DB_NAME`

#### Deprecated Postgresql-specific Environment variables :
- `POSTGRES_ENV_POSTGRES_USER` : equivalent of (and superseded by) `SQTM_DB_USERNAME`
- `POSTGRES_ENV_POSTGRES_PASSWORD` : equivalent of (and superseded by) `SQTM_DB_PASSWORD`
- `POSTGRES_ENV_POSTGRES_DB` : equivalent of (and superseded by) `SQTM_DB_NAME`


## Other useful variables

Squash-TM is a Spring Boot application; as such it benefits from its flexible configuration mechanism 
([documentation here](https://docs.spring.io/spring-boot/docs/2.1.6.RELEASE/reference/html/boot-features-external-config.html)).
We describe here two environment variables that could help you to configure your instance and possibly eliminate the need 
to manage the usual configuration files.

In addition to the properties specific to Squash-TM, remember that the Spring platform itself can be configured too. 
The reference can be [found here](https://docs.spring.io/spring-boot/docs/2.1.6.RELEASE/reference/html/common-application-properties.html)
and can help you adapt your container to your infrastructure, especially the server properties (which begins with the prefix `server.`)

### Timezone
You can adjust your container's timezone settings with the TZ environment variable of the package tzdata, for example:

TZ=Europe/Paris

### App config using JAVA_TOOL_OPTIONS

This standard variable in the Java world allows to customize the JVM process even when the command line 
is not readily available (which is the case here) and can be exploited to eg configure access through 
authenticated proxies etc. Although originally intended to JVM tuning only it can also be leveraged for application configuration. 
Any property that could be defined in the regular configuration files can be defined in that environment variable instead. 
Each pair of `propery=value` must be prefixed with `-D`, and be separated by a space or other separator character. 

There are a few caveats however. Frustratingly it cannot be used yet to configure the heap size (other JVM flags work fine). 
Another shortcoming is that it cannot be supplemented; only redeclared. The Dockerfile sets its default  
 to `-Djava.awt.headless=true`, which is required because Squash-TM runs in a non-GUI Linux environment. If you need 
 to override it please remember to manually add this parameter back (otherwise you may experience troubles when generating 
 and exporting reports). These inconveniences originate from archaisms in the startup script and will be addressed 
 in future releases.

Example: Running Squash-TM with the Fiji timezone and enable all Spring Boot actuator endpoints (use with caution, 
        this will exposes sensitive settings !)

    export SQTM_OPTS="-Djava.awt.headless=true \
                        -Duser.timezone=Pacific/Fiji \
                        -Dmanagement.endpoints.enabled-by-default=true \
                        -Dmanagement.endpoints.web.exposure.include=*"
                        
    docker run --rm -d -p 8080:8080 -e JAVA_TOOL_OPTIONS="$SQTM_OPTS" squashtest/squash-tm

### App config using SPRING_APPLICATION_JSON

This environment variable is another way to convey the configuration to the application. As the name implies the 
configuration should be provided as a JSON string. Unlike the variable `JAVA_TOOL_OPTIONS`, `SPRING_APPLICATION_JSON` 
cannot be used for JVM tuning (ie it supports TM and Spring properties only), but offers interesting possibilities and 
more semantically satisfying than hacking around with `JAVA_TOOL_OPTIONS`.

Example: enable support for SSL offload

    cat << EOF > tmconf.json
    {
        "server": {
            "use-forward-headers": true
        }
    }
    EOF
    
    docker run --rm -d -p 8080:8080 -e SPRING_APPLICATION_JSON="$(jq -c . < tmconf.json)" squashtest/squash-tm 


## Reporting bugs

Please send your reports to [https://ci.squashtest.org/](https://ci.squashtest.org/) or `contact@squashtest.com`.
Along with the relevant information (environment, steps to reproduce etc) you should also mention the value of the Docker label
`DOCKER_BUILD_GIT_REVISION` if exist (from 1.21.4 onward) :

    docker inspect <your image> | grep DOCKER_BUILD_GIT_REVISION

# Deploying Squash TM

## With PostgreSQL

The database is created by the database container and automatically populated by the application container on first run, 
or upgraded as necessary when a newer version of Squash-TM is deployed.

All data from the database will be saved within the local volume named ‘squash-tm-db-pg’. So the db container 
(called ‘squash-tm-pg’) can be stop and restart with no risk of losing them.
```
docker run -it -d --name='squash-tm-pg' \
-e POSTGRES_USER=squashtm \
-e POSTGRES_PASSWORD=MustB3Ch4ng3d \
-e POSTGRES_DB=squashtm \
-v squash-tm-db-pg:/var/lib/postgresql/data \
postgres:9.6.12 \

sleep 10

docker run -it -d --name=squash-tm \
--link squash-tm-pg:postgres \
-v squash-tm-logs:/opt/squash-tm/logs -v squash-tm-plugins:/opt/squash-tm/plugins \
-p 8090:8080 \
squashtest/squash-tm
```
Wait 3-4 minutes the time for Squash-TM to initialize. then log in to *http://localhost:8090/squash* (admin  /  admin)

## With MariaDB

Database is created by the database container and automatically populated by the application container on first run, 
or upgraded as necessary when a newer version of Squash-TM is deployed.

All data from the database will be saved within the local volume named ‘squash-tm-db-mdb’. So the db container 
(called ‘squash-tm-mdb’) can be stop and restart with no risk of losing them.
```
docker run -it -d --name='squash-tm-mdb' \
-e MYSQL_ROOT_PASSWORD=MustB3Ch4ng3d \
-e MYSQL_USER=squashtm \
-e MYSQL_PASSWORD=MustB3Ch4ng3d \
-e MYSQL_DATABASE=squashtm \
-v squash-tm-db-mdb:/var/lib/mysql \
mariadb:10.2.22-bionic --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci \

sleep 10

docker run -it -d --name=squash-tm \
--link squash-tm-mdb:mysql \
-v squash-tm-logs:/opt/squash-tm/logs -v squash-tm-plugins:/opt/squash-tm/plugins \
-p 8090:8080 \
squashtest/squash-tm
```
Wait several minutes while Squash-TM initializes the database (mysql initialisation is significantly longer than postres). 
Then log in to *http://localhost:8090/squash* (admin  /  admin).


## Docker-Compose

### `docker-compose.yml file`

The following example of a docker-compose.yml link squash-tm to a MariaDB database. The environment variables should be set in a .env file (saved in the same repositority as the docker-compose.yml)

```
version: '3.7'
services:
  squash-tm-md:
    image: mariadb:10.2.22-bionic
    environment:
      MYSQL_ROOT_PASSWORD: ${DB_ROOT_PASSWORD}
      MYSQL_USER: ${DB_USER}
      MYSQL_PASSWORD: ${DB_PASSWORD}
      MYSQL_DATABASE: ${DB_DATABASE}
    volumes:
      - "/var/lib/mysql-db:/var/lib/mysql"

  squash-tm:
    image: squashtest/squash-tm
    depends_on:
      - squash-tm-md
    environment:
      SQTM_DB_TYPE: mysql
      SQTM_DB_USERNAME: ${DB_USER}
      SQTM_DB_PASSWORD: ${DB_PASSWORD}
      SQTM_DB_NAME: ${DB_DATABASE}
    ports:
      - 8090:8080/tcp
    links:
      - squash-tm-md:mysql
    volumes:
      - squash-tm-logs:/opt/squash-tm/logs
      - squash-tm-plugins:/opt/squash-tm/plugins

volumes:
  squash-tm-logs:
  squash-tm-plugins:
```

And the `.env` file :

```
DB_USER=squashtm
DB_PASSWORD=MustB3Ch4ng3d
DB_DATABASE=squashtm
```


### Run a docker-compose

1. Copy the **docker-compose.yml** that correspond to your need.
You’ll find several docker-compose repository on our [Bitbucket](https://bitbucket.org/squashtest/docker-squash-tm/src/master/)  :
 - [Squash-tm deployment using MariaDB database](https://bitbucket.org/squashtest/docker-squash-tm/src/master/docker-compose-mariadb/)
 - [Squash-tm deployment using Postgres database](https://bitbucket.org/squashtest/docker-squash-tm/src/master/docker-compose-postgres/)
 - [Squash-tm deployment using Postgres database and Reverse-Proxy](https://bitbucket.org/squashtest/docker-squash-tm/src/master/docker-compose-reverseproxy/docker-compose-nginx-postgres/)
 - [Squash-tm deployment using MariaDB database and Reverse-Proxy](https://bitbucket.org/squashtest/docker-squash-tm/src/master/docker-compose-reverseproxy/docker-compose-nginx-mariadb/)

2. Don’t forget to create a **.env** file (or set the value of environment variables directly in the docker-compose.yml file).

3. In the docker-compose.yml directory, run ``` docker-compose up ``` or ``` docker-compose up -d ``` for daemon mode.

4. Log in to *http://localhost:8090/squash* or  *http://**{host_ip}**:8090/squash*

For more information about docker-compose here is the [documentation](https://docs.docker.com/compose/)

# Appendix
## Databases Environment variables

Since we often use images of existing DB container here we highlight a selection of relevant environment variable for 
convenience, and links leading to their documentation. 

### Postgres image environment variables

#### `POSTGRES_PASSWORD`

This environment variable sets the superuser password for PostgreSQL. The default superuser is defined by the 
`POSTGRES_USER` environment variable.

#### `POSTGRES_USER`

This optional environment variable is used in conjunction with `POSTGRES_PASSWORD` to set a user and its password. 
This variable will create the specified user with superuser power and a database with the same name. 
If it is not specified, then the default user of `postgres` will be used.

#### `POSTGRES_DB`

This optional environment variable can be used to define a different name for the default database 
created when it runs for the first time. If it is not specified, then the value of `POSTGRES_USER` will be used.

For further information and optional environment variables, 
please check out the [Posgres image documentation](https://hub.docker.com/_/postgres)


### MariaDB image environment variables

#### `MYSQL_ROOT_PASSWORD`
This variable is mandatory and specifies the password that will be set for the MariaDB `root` superuser account.

#### `MYSQL_DATABASE`
This variable is optional and allows you to specify the name of a database to be created on image startup. If a user/password 
was supplied then that user will be granted superuser access ([corresponding to `GRANT ALL`](http://dev.mysql.com/doc/en/adding-users.html)) to this database.

#### `MYSQL_USER`, `MYSQL_PASSWORD`

These variables are optional, used in conjunction to create a new user and to set that user's password. This user will be granted superuser permissions (see above) for the database specified by the `MYSQL_DATABASE` variable. Both variables are required for a user to be created.

Do note that there is no need to use this mechanism to create the root superuser, that user gets created by default with the password specified by the `MYSQL_ROOT_PASSWORD` variable.

For further information and optional environment variables, please check out the [MariaDB image documentation](https://hub.docker.com/_/mariadb)

## Using Squash TM container with a reverse proxy

Two examples of docker-compose.yml deploying Squash TM behind a reverse proxy are available on our ** [Bitbucket](https://bitbucket.org/squashtest/docker-squash-tm/src/master/) ** :

 - [Squash-tm deployment using Postgres database and Reverse-Proxy](https://bitbucket.org/squashtest/docker-squash-tm/src/master/docker-compose-reverseproxy/docker-compose-nginx-postgres/)
 - [Squash-tm deployment using MariaDB database and Reverse-Proxy](https://bitbucket.org/squashtest/docker-squash-tm/src/master/docker-compose-reverseproxy/docker-compose-nginx-mariadb/)

These solutions use a [docker image from jwilder based on nginx-proxy](https://hub.docker.com/r/jwilder/nginx-proxy).

Here is an example of Squash TM deployed behind a reverse-proxy using Postres database:
```
version: '3.7'
services:
  squash-tm-pg:
    container_name: squash-tm-pg
    environment:
      POSTGRES_DB: ${DB_DATABASE}
      POSTGRES_PASSWORD: ${DB_PASSWORD}
      POSTGRES_USER: ${DB_USER}
    image: postgres:9.6.12
    volumes:
      - /var/lib/db-postgresql:/var/lib/postgresql/data
    networks:
      - db-network

  squash-tm:
    depends_on:
      - squash-tm-pg
    environment:
      SQTM_DB_USERNAME: ${DB_USER}
      SQTM_DB_PASSWORD: ${DB_PASSWORD}
      SQTM_DB_NAME: ${DB_DATABASE}
      VIRTUAL_HOST: mysquash.example.com
    ports:
      - 8090:8080/tcp
    image: squashtest/squash-tm
    links:
      - squash-tm-pg:postgres
    volumes:
      - squash-tm-logs:/opt/squash-tm/logs
      - squash-tm-plugins:/opt/squash-tm/plugins
    networks:
      - nginx-proxy
      - db-network

  nginx-proxy:
    container_name: nginx
    image: jwilder/nginx-proxy
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro
    networks:
      - nginx-proxy

volumes:
  squash-tm-logs:
  squash-tm-plugins:

networks:
  nginx-proxy:
  db-network:

```


**References**

-  [Squash-TM documentation](https://sites.google.com/a/henix.fr/wiki-squash-tm/)
-  [Spring Boot properties](https://docs.spring.io/spring-boot/docs/2.1.6.RELEASE/reference/html/common-application-properties.html)
-  [Bitbucket repository of Squash TM docker sources](https://bitbucket.org/squashtest/docker-squash-tm/src/)
-  [Posgres image documentation](https://hub.docker.com/_/postgres)
-  [MariaDB image documentation](https://hub.docker.com/_/mariadb)
-  [jwilder/nginx-proxy documentation](https://hub.docker.com/r/jwilder/nginx-proxy)

