#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2010 - 2012 Henix, henix.fr
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses/>.
#

# #############################################################################################
#
#   Database Environment variables:
#
#
#  | Variable        | Description                           | Default H2  | Default MySQL | Default Postgresql  |
#  | ----------------|:-------------------------------------:|:-----------:|:-------------:|:-------------------:|
#  | SQTM_DB_TYPE    | Either 'h2', 'mysql' or 'postgresql'  | (special)   | (special)     | (special)           |
#  | SQTM_DB_HOST    | Hostname of the database server       | (unused)    | mysql         | postgres            |
#  | SQTM_DB_PORT    | Port of the database sever            | (unused)    | 3306          | 5432                |
#  | SQTM_DB_NAME    | The name of the database              | (unused)    | squashtm      | squashtm            |
#  | SQTM_DB_SCHEMA *| The name of the schema                | (unused)    | $DB_NAME      | public              |
#  | SQTM_DB_USERNAME| The username for Squash-TM            | (unused)    | root          | root                |
#  | SQTM_DB_PASSWORD| The password for Squash-TM            | (unused)    | (none)        | (none)              |
#
#   notes about default values:
#   - (none) : the variable is mandatory and has no default value.
#   - (special) : see below.
#   - (*) : experimental, should be stable but has not yet gone through thorough testing.
#   - (unused) :  the variable has no meaning for this database server.
#                 In the case of H2 those values are hardcoded to Squash-TM default internal parameters.
#
#   Variable SQTM_DB_TYPE and deprecated variables:
#   -----------------------------------------------
#   'SQTM_DB_TYPE' has an impact on the default values for several others. The best practice is to define it explicitly.
#   However in previous version of the image this variable was internal only and its value was implied by the existence
#   of other variables which are now legacy (see below).
#   This mechanism is still maintained for now, eg if 'SQTM_DB_TYPE' is undefined but any variable of the form
#   'MYSQL_ENV_*' or 'POSTGRES_ENV_*' then the value of 'SQTM_DB_TYPE' will be implicitly resolved. However, variables
#   of the form 'SQTM_*' will take precedence. Also be aware that this secondary mechanism will effectively stop working
#   once the deprecated variables are removed eventually.
#
#
#   Deprecated Environment Variables :
#   ------------------------------
#
#   The variable 'MYSQL_ENV_MYSQL_ROOT_PASSWORD' has been removed and is not supported anymore.
#
#   The following variable are still supported, although we strongly recommend to move to the new variables.
#
#   Deprecated MySQL/MariaDB-specific Environment variables :
#
#   - MYSQL_ENV_MYSQL_USER : the username of Squash-TM service account on MySQL
#   - MYSQL_ENV_MYSQL_PASSWORD : the password that goes along that user account
#   - MYSQL_ENV_MYSQL_DATABASE : the name of the schema on MySQL
#
#   Deprecated Postgresql-specific Environment variables :
#
#   - POSTGRES_ENV_POSTGRES_USER : the username of Squash-TM service account on Postgres
#   - POSTGRES_ENV_POSTGRES_PASSWORD : the password that goes along that user account
#   - POSTGRES_ENV_POSTGRES_DB : the name of the database on the postgresql cluster (server)
#
#
#
# #############################################################################################

cd /opt/squash-tm/bin

########## SEARCHING FOR LINKED DB SERVER ############
##########      PREPARING CONNECTION      ############

# Locate if the legacy variables are set
LEGACY_MYSQL_TYPE=$(env | grep MYSQL_ENV | head -n 1 | sed s/.*/mysql/g)
LEGACY_PG_TYPE=$(env | grep POSTGRES_ENV | head -n 1 | sed s/.*/postgresql/g)

# Chain resolution of DB_TYPE by order of priority :
# 1/ variable explicitly set,
# 2/ either 'mysql' or 'postgresql' if legacy variables are set
# 3/ h2 as default
DB_TYPE=${SQTM_DB_TYPE:-${LEGACY_MYSQL_TYPE:-${LEGACY_PG_TYPE:-'h2'}}}

case "$DB_TYPE" in
    "postgresql")
      DB_HOST=${SQTM_DB_HOST:-'postgres'}
      DB_PORT=${SQTM_DB_PORT:-'5432'}
      DB_NAME=${SQTM_DB_NAME:-${POSTGRES_ENV_POSTGRES_DB:-'squashtm'}}
      DB_USERNAME=${SQTM_DB_USERNAME:-${POSTGRES_ENV_POSTGRES_USER:-'root'}}
      DB_PASSWORD=${SQTM_DB_PASSWORD:-${POSTGRES_ENV_POSTGRES_PASSWORD}}
      DB_SCHEMA="${SQTM_DB_SCHEMA:-public}"

      DB_URL="jdbc:${DB_TYPE}://${DB_HOST}:${DB_PORT}/$DB_NAME?currentSchema=$DB_SCHEMA"
      ;;
    "mysql")
      DB_HOST=${SQTM_DB_HOST:-'mysql'}
      DB_PORT=${SQTM_DB_PORT:-'3306'}
      DB_NAME=${SQTM_DB_NAME:-${MYSQL_ENV_MYSQL_DATABASE:-'squashtm'}}
      DB_USERNAME=${SQTM_DB_USERNAME:-${MYSQL_ENV_MYSQL_USER:-'root'}}
      DB_PASSWORD=${SQTM_DB_PASSWORD:-${MYSQL_ENV_MYSQL_PASSWORD}}
      DB_SCHEMA="${SQTM_DB_SCHEMA:-${DB_NAME}}"

      DB_URL="jdbc:${DB_TYPE}://${DB_HOST}:${DB_PORT}/$DB_NAME"
      ;;
    "h2")
      echo 'his squash instance will use the default embedded h2 DB. This is OK for test purpose only'.
      echo 'TO USE SQUASH TM IN PRODUCTION, IT IS RECOMMENDED TO USE EITHER MYSQL OR POSTGESQL DATABASE'
      DB_HOST='..'
      DB_USERNAME='sa'
      DB_PASSWORD='sa'
      DB_NAME='squashtm'
      DB_URL='jdbc:h2:../data/squash-tm'
      ;;
esac

# validate that a password was supplied
echo ${DB_PASSWORD:?'Error : no database password is set'} > /dev/null

cat << EOF
Resolved configuration :

DB_TYPE       $DB_TYPE
DB_HOST       $DB_HOST
DB_PORT       $DB_PORT
DB_NAME       $DB_NAME
DB_SCHEMA     $DB_SCHEMA
DB_USERNAME   $DB_USERNAME
DB_PASSWORD   ************
DB_URL        $DB_URL
EOF

# Also, export the resolved configuration in /opt/squash-tm/bin/docker-resolved-env for convenience
cat << EOF > /opt/squash-tm/bin/docker-resolved-env
export DB_TYPE="$DB_TYPE"
export DB_HOST="$DB_HOST"
export DB_PORT="$DB_PORT"
export DB_NAME="$DB_NAME"
export DB_SCHEMA="$DB_SCHEMA"
export DB_USERNAME="$DB_USERNAME"
export DB_PASSWORD="$DB_PASSWORD"
export DB_URL="$DB_URL"
EOF

echo
echo


########## CHECKING SQUASH TM DB EXISTENCE ###########
##########       EXECUTING DB SCRIPTS      ###########

####### Definitions

# Parameters : none
wait_for_db(){
  case "$DB_TYPE" in
    'mysql') until mysqladmin status --protocol tcp -u "$DB_USERNAME" -p"$DB_PASSWORD" -h "$DB_HOST" -P "$DB_PORT"; do echo waiting for mysql; sleep 2; done; ;;
    'postgresql') until pg_isready -d ${DB_NAME} -h ${DB_HOST} -p ${DB_PORT} -U ${DB_USERNAME} -t 5; do echo waiting for postgresql; sleep 2; done; ;;
  esac
}

# Parameters : $1 the path to the script to run
run_script(){
  case "$DB_TYPE" in
    'mysql') mysql --protocol tcp  -h "$DB_HOST" -u "$DB_USERNAME" -p"$DB_PASSWORD" -P "$DB_PORT" "$DB_NAME" < "$1" ;;
    'postgresql')
      PGOPTIONS="--search_path=${DB_SCHEMA}"  psql postgresql://$DB_USERNAME:$DB_PASSWORD@$DB_HOST:${DB_PORT}/$DB_NAME -f "$1" ;;
  esac
}

# Parameters : $1 the query
run_query(){
  case "$DB_TYPE" in
    'mysql') mysql --protocol tcp  -h "$DB_HOST" -u "$DB_USERNAME" -p"$DB_PASSWORD" -P "$DB_PORT" "$DB_NAME" -e "$1" ;;
    'postgresql')
      PGOPTIONS="--search_path=${DB_SCHEMA}"  psql postgresql://$DB_USERNAME:$DB_PASSWORD@$DB_HOST:${DB_PORT}/$DB_NAME -c "$1" ;;
  esac
}

find_db_upgrade_scripts(){

  DB_SCHEM_VERSION=$(run_query "SELECT * FROM CORE_CONFIG WHERE STR_KEY='squashtest.tm.database.version';" |  grep -Eo '(\d+\.?){2,}')

  # find all the scripts
  find /opt/squash-tm/database-scripts | \
    # retain only those that corresponds to this DB_TYPE and are upgrades scripts
    grep "${DB_TYPE}-upgrade" |  \
    # prepend the output with version numbers, which will be our sorting keys  : "<major> <minor> <micro> <scriptname>"
    sed -E 's/(.*(\d+).(\d+).(\d+).sql)/\2 \3 \4 \1/g' | \
    # left-pad with zeroes the version numbers so that a lexicographic sort will work just like semantic versioning
    awk '{printf "%05d %05d %05d %s\n", $1, $2, $3, $4;}' | \
    # now we can sort
    sort | \
    # remove the sorting key
    cut -d ' ' -f 4 | \
    # retain all the scripts that match the current schema version and all those after it
    grep -F "$DB_SCHEM_VERSION" -A 10000 | \
    # remove the first script of the current schema version
    awk 'NR>1'
}


###### Execution


echo "testing : is database present"
echo "Targeting ${DB_HOST}:${DB_PORT}($DB_TYPE)"
echo "Waiting until database is available..."

wait_for_db

echo 'Database is up. Is it new ?'

# Mysql likes its tablenames uppercase while PG likes it lowercase
QUERY_DB_EXIST="SELECT 1 FROM information_schema.tables WHERE table_schema = '${DB_SCHEMA}' AND (table_name = 'ISSUE' OR table_name = 'issue');"
if ! run_query "$QUERY_DB_EXIST" | grep 1 > /dev/null; then
  echo 'The database is empty, will now initialize it'

  FULL_INSTALL_SCRIPT=$(find /opt/squash-tm/database-scripts/ | grep "$DB_TYPE" | grep full-install)
  run_script "$FULL_INSTALL_SCRIPT"

  echo 'Database is initialized'
else
  echo "An existing schema was found. Is an upgrade necessary ?"

  PENDING_UPGRADES=$(find_db_upgrade_scripts)

  if [ "${#PENDING_UPGRADES}" = 0 ]; then
    echo "Database already up to date"
  else
    echo "unrolling scripts : "
    for sc in $PENDING_UPGRADES; do
      echo applying "$sc";
      run_script "$sc"
    done
    echo "Database upgrade complete"
  fi
fi



########## MODIFYING DB CONNECTION VARIABLES ###########
##########    IN SQUASH-TM STARTUP SCRIPT    ###########

escape_slashes(){
  echo "$1" | sed 's?/?\\/?g'
}


echo 'Updating startup.sh'
cd /opt/squash-tm/bin
sed -i "s/^DB_TYPE=.*/DB_TYPE=$(escape_slashes $DB_TYPE)/" startup.sh &&\
sed -i "s/^DB_URL=.*/DB_URL=$(escape_slashes $DB_URL)/" startup.sh &&\
sed -i "s/^DB_USERNAME=.*/DB_USERNAME=$(escape_slashes $DB_USERNAME)/" startup.sh &&\
sed -i "s/^DB_PASSWORD=.*/DB_PASSWORD=$(escape_slashes $DB_PASSWORD)/" startup.sh


# Starting up squash-tm
cd /opt/squash-tm/bin && ./startup.sh
